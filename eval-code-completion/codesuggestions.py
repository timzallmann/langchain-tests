
from flask import Flask, request

import os, yaml, json
from getpass import getpass 
from langchain import LLMChain, PromptTemplate, SerpAPIWrapper
from langchain.agents import (
    AgentExecutor, 
    AgentOutputParser,
    AgentType, 
    ConversationalChatAgent, 
    LLMSingleActionAgent, 
    Tool, 
    create_json_agent, 
    initialize_agent 
)
from langchain.agents.agent import AgentExecutor 
from langchain.agents.agent_toolkits import JsonToolkit
from langchain.agents.agent_toolkits.json.prompt import JSON_PREFIX, JSON_SUFFIX  
from langchain.agents.agent_toolkits.json.toolkit import JsonToolkit
from langchain.agents.mrkl.base import ZeroShotAgent
from langchain.agents.mrkl.prompt import FORMAT_INSTRUCTIONS
from langchain.base_language import BaseLanguageModel
from langchain.callbacks import StdOutCallbackHandler
from langchain.callbacks.base import BaseCallbackHandler, BaseCallbackManager
from langchain.callbacks.manager import (
    AsyncCallbackManagerForToolRun, 
    CallbackManagerForToolRun 
)
from langchain.chains import LLMChain
from langchain.chains.llm import LLMChain
from langchain.chat_models import ChatAnthropic, ChatOpenAI, ChatVertexAI 
from langchain.llms import VertexAI
from langchain.memory import ConversationBufferMemory
from langchain.prompts import BaseChatPromptTemplate
from langchain.prompts.chat import (
    ChatPromptTemplate,
    AIMessagePromptTemplate, 
    HumanMessagePromptTemplate,
    SystemMessagePromptTemplate
) 
from langchain.requests import TextRequestsWrapper
from langchain.schema import AgentAction, AgentFinish, HumanMessage, AIMessage
from langchain.tools import BaseTool, StructuredTool, Tool
from langchain.tools.json.tool import (
    JsonGetValueTool, 
    JsonListKeysTool, 
    JsonSpec
)
from pydantic import BaseModel, Field
from typing import Any, Dict, List, Optional, Type 
from typing import List, Union 

from vertexainew import VertexAINew

from gitlabcodegen import complete_gitlab_native_code
from anthropiccode import complete_anthropic_code
from vertexaicode import complete_vertex_text_bison, complete_vertex_code_bison, complete_vertex_gecko, complete_vertex_code

from codeutils import Timer, get_code_from_md_block

import re
import yaml

token_target_creation = 256
token_target_completion = 128

def test_run(test_function, language, code, **kwargs):
    t = Timer()
    t.start()
    result_code = test_function(language, code, **kwargs)
    duration = t.stop()

    if result_code:
        return {"result": result_code, "duration": duration, "result_length": len(result_code)}
    else:
        return {"result": ""}

def test_code_example(element):
    print(element["user_intention"])
    test_code = element["prompt"]
    element["anthropic"] = test_run(complete_anthropic_code, element["language"], test_code)
    print('\n\n')
    element["anthropic_fast"] = test_run(complete_anthropic_code, element["language"], test_code, use_fast=True)
    print('\n\n')
    element["vertex_combined"] = test_run(complete_vertex_code, element["language"], test_code)
    print('\n\n')
    element["vertex_code_bison"] = test_run(complete_vertex_code_bison, element["language"], test_code)
    print('\n\n')
    element["vertex_text_bison"] = test_run(complete_vertex_text_bison, element["language"], test_code)
    print('\n\n')
    element["vertex_gecko"] = test_run(complete_vertex_gecko, element["language"], test_code)
    print('\n\n')
    element["gitlab_model"] = test_run(complete_gitlab_native_code, element["language"], test_code)
    print(element)

# Setup flask server
app = Flask(__name__) 
  
@app.route('/suggestions', methods = ['GET', 'POST']) 
def ask_result(): 
    data = request.json
    print("LANGUAGE : " + data.get("language"))
    print(data.get("source"))

    test_code = {"user_intention": "Live Test", "language": data.get("language"), "prompt": data.get("source")}
    test_code_example(test_code)
    print('Sending Result')
    print(test_code)
    return json.dumps(test_code)
if __name__ == "__main__": 
    app.run(port=5000, debug=True)
