const fs = require('fs');
const bodyParser = require('body-parser');
const app = require('express')();
const axios = require('axios');

const rawData = fs.readFileSync('../eval-code-completion/test-results.json');
const testResults = JSON.parse(rawData);

app.use(bodyParser.json());

app.all('/results', (req, res) => {
  return res.json(testResults);
});

app.all('/suggestions', async (req, res) => {
  console.log('SUGGESTIONS : ', req.body);

  if (req.body.source && req.body.language) {
    try {
      const response = await axios.post('http://127.0.0.1:5000/suggestions', req.body);
      console.log(response.data);
      res.json(response.data);
    } catch (error) {
      console.error(error);
      res.json(error);
    }
  }
});

module.exports = app;
