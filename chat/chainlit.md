# Welcome to the GitLab Chat POC Anthropic

This is a test for testing out 2 different providers and our system message templates.

It simulates being on an issue page and a code file tool, that you can ask questions about. Also base questions about the GitLab project can be asked.

- **Example Issue:**  <https://gitlab.com/gitlab-org/gitlab/-/issues/9597>
- **Example Codefile:**  <https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/assets/javascripts/autosave.js>


## Example questions

To modify the welcome screen, edit the `chainlit.md` file at the root of your project. If you do not want a welcome screen, just leave this file empty.

## Code File

- Can you explain the code? 
- Can you explain function X?
- Write me tests for function X
- What is the complexity of the code
- How would the code look like in Python?
- How would you refactor the code?
- Can you fix the bug in my code
- Create an example of how to use function X
- Write documentation for the selected code
- Create a function to validate an e-mail address
- Create a tic tac toe game in Javascript
- Create a function in Python to call the spotify API to get my playlists (Needs to be precise that it should show a function and not create one as it then says can't create one.

## Issue

- Please summarize the issue
- Summarize the issue with bullet points
- Can you list all the labels? (Only worked with Can you list all the labels of the issue?)
- How old is the issue?
- For which milestone is the issue? And how long until then
- Questions on the actual content: "What should be the final solution for this issue?" (Worked then with - For the issue what shall be the final solution contain?)
