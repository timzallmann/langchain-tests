import chainlit as cl

import os, yaml, json

# os.environ['OPENAI_API_KEY'] = ""  # @param {type:"string"}
# os.environ['ANTHROPIC_API_KEY'] = ""  # @param {type:"string"}
# os.environ['GOOGLE_API_KEY'] = ""  # @param {type:"string"}

example_issue_url = "https://gitlab.com/gitlab-org/gitlab/-/issues/9597"  # @param {type:"string"}

# !curl -O https://gitlab.com/timzallmann/langchain-tests/-/raw/main/chat/agent.py
# !curl -O https://gitlab.com/timzallmann/langchain-tests/-/raw/main/chat/zero_shot_agent.py
# !curl -O https://gitlab.com/timzallmann/langchain-tests/-/raw/main/chat/code_file.json

# !wget -O issue.json $example_issue_url\.json
# !wget -O project.json https://gitlab.com/api/v4/projects/278964

from langchain.agents import (
    create_json_agent,
    AgentExecutor,
    AgentType,
    initialize_agent,
    Tool
)
from langchain.memory import ConversationBufferMemory
from langchain.agents.agent_toolkits import JsonToolkit
from langchain.chains import LLMChain
from langchain import PromptTemplate
from langchain.llms.openai import OpenAI
from langchain.chat_models import ChatOpenAI, ChatAnthropic, ChatVertexAI
from langchain.requests import TextRequestsWrapper
from langchain.tools.json.tool import JsonGetValueTool, JsonListKeysTool, JsonSpec
from langchain.tools import BaseTool, StructuredTool, Tool, tool
from langchain.callbacks.base import BaseCallbackHandler

from typing import Any, Dict, List, Optional

from langchain.agents.agent import AgentExecutor
from langchain.agents import initialize_agent, Tool, ConversationalChatAgent
from langchain.agents.agent_toolkits.json.prompt import JSON_PREFIX, JSON_SUFFIX
from langchain.agents.agent_toolkits.json.toolkit import JsonToolkit
from langchain.agents.mrkl.base import ZeroShotAgent
from langchain.agents.mrkl.prompt import FORMAT_INSTRUCTIONS
from langchain.base_language import BaseLanguageModel
from langchain.callbacks.base import BaseCallbackManager
from langchain.chains.llm import LLMChain

from langchain.agents import Tool, AgentExecutor, LLMSingleActionAgent, AgentOutputParser
from langchain.prompts import BaseChatPromptTemplate
from langchain import SerpAPIWrapper, LLMChain
from langchain.chat_models import ChatOpenAI
from typing import List, Union
from langchain.schema import AgentAction, AgentFinish, HumanMessage
import re
from getpass import getpass

from langchain.prompts.chat import (
    ChatPromptTemplate,
    SystemMessagePromptTemplate,
    AIMessagePromptTemplate,
    HumanMessagePromptTemplate,
)

from typing import Any, Dict, List, Optional

from pydantic import Extra

from langchain.callbacks.manager import (
    AsyncCallbackManagerForLLMRun,
    CallbackManagerForLLMRun,
)
from langchain.chat_models.base import BaseChatModel
from langchain.llms.anthropic import _AnthropicCommon
from langchain.schema import (
    AIMessage,
    BaseMessage,
    ChatGeneration,
    ChatMessage,
    ChatResult,
    HumanMessage,
    SystemMessage,
)


class ChatAnthropicNew(BaseChatModel, _AnthropicCommon):
    r"""Wrapper around Anthropic's large language model.

    To use, you should have the ``anthropic`` python package installed, and the
    environment variable ``ANTHROPIC_API_KEY`` set with your API key, or pass
    it as a named parameter to the constructor.

    Example:
        .. code-block:: python
            import anthropic
            from langchain.llms import Anthropic
            model = ChatAnthropic(model="<model_name>", anthropic_api_key="my-api-key")
    """

    class Config:
        """Configuration for this pydantic object."""

        extra = Extra.forbid

    @property
    def _llm_type(self) -> str:
        """Return type of chat model."""
        return "anthropic-chat"

    def _convert_one_message_to_text(self, message: BaseMessage) -> str:
        if isinstance(message, ChatMessage):
            message_text = f"\n\n{message.role.capitalize()}: {message.content}"
        elif isinstance(message, HumanMessage):
            message_text = f"{self.HUMAN_PROMPT} {message.content}"
        elif isinstance(message, AIMessage):
            message_text = f"{self.AI_PROMPT} {message.content}"
        elif isinstance(message, SystemMessage):
            message_text = f"{self.HUMAN_PROMPT} <admin>{message.content}</admin>"
        else:
            raise ValueError(f"Got unknown type {message}")
        return message_text

    def _convert_messages_to_text(self, messages: List[BaseMessage]) -> str:
        """Format a list of strings into a single string with necessary newlines.

        Args:
            messages (List[BaseMessage]): List of BaseMessage to combine.

        Returns:
            str: Combined string with necessary newlines.
        """
        return "".join(
            self._convert_one_message_to_text(message) for message in messages
        )

    def _convert_messages_to_prompt(self, messages: List[BaseMessage]) -> str:
        """Format a list of messages into a full prompt for the Anthropic model

        Args:
            messages (List[BaseMessage]): List of BaseMessage to combine.

        Returns:
            str: Combined string with necessary HUMAN_PROMPT and AI_PROMPT tags.
        """
        if not self.AI_PROMPT:
            raise NameError("Please ensure the anthropic package is loaded")

        if not isinstance(messages[-1], AIMessage):
            messages.append(AIMessage(content=""))
        text = self._convert_messages_to_text(messages)
        return (
            text.rstrip()
        )  # trim off the trailing ' ' that might come from the "Assistant: "

    def _generate(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
    ) -> ChatResult:
        prompt = self._convert_messages_to_prompt(messages)
        params: Dict[str, Any] = {"prompt": prompt, **self._default_params}
        if stop:
            print("Anthropic Stop Values " + str(stop))
            if stop[0] != "\nObservation:":
                params["stop_sequences"] = stop

        print("ANTHROPIC REQUEST : " + str(params))

        if self.streaming:
            completion = ""
            stream_resp = self.client.completion_stream(**params)
            for data in stream_resp:
                delta = data["completion"][len(completion) :]
                completion = data["completion"]
                if run_manager:
                    run_manager.on_llm_new_token(
                        delta,
                    )
        else:
            response = self.client.completion(**params)
            completion = response["completion"]

        print("ANTHROPIC RESPONSE : " + str(completion))
        
        message = AIMessage(content=completion)
        return ChatResult(generations=[ChatGeneration(message=message)])

    async def _agenerate(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        run_manager: Optional[AsyncCallbackManagerForLLMRun] = None,
    ) -> ChatResult:
        prompt = self._convert_messages_to_prompt(messages)
        params: Dict[str, Any] = {"prompt": prompt, **self._default_params}
        if stop:
            params["stop_sequences"] = stop

        if self.streaming:
            completion = ""
            stream_resp = await self.client.acompletion_stream(**params)
            async for data in stream_resp:
                delta = data["completion"][len(completion) :]
                completion = data["completion"]
                if run_manager:
                    await run_manager.on_llm_new_token(
                        delta,
                    )
        else:
            response = await self.client.acompletion(**params)
            completion = response["completion"]
        message = AIMessage(content=completion)
        return ChatResult(generations=[ChatGeneration(message=message)])

    def get_num_tokens(self, text: str) -> int:
        """Calculate number of tokens."""
        if not self.count_tokens:
            raise NameError("Please ensure the anthropic package is loaded")
        return self.count_tokens(text)

"""Setting up different LLM to test against, you can change the `selected_llm` to different values to use different providers/models"""


from typing import Optional, Type
from pydantic import BaseModel, Field

from langchain.callbacks.manager import AsyncCallbackManagerForToolRun, CallbackManagerForToolRun

from langchain.callbacks import StdOutCallbackHandler

from zero_shot_agent import ZeroShotAgentGL

import yaml

@cl.langchain_factory
def factory():

    # llm_openai = ChatOpenAI(model_name="gpt-4", temperature=0)

    # llm_google = ChatVertexAI(temperature=0)

    llm_anthropic = ChatAnthropicNew(model="claude-instant-v1", temperature="0", max_tokens_to_sample=8000)

    # 

    # llm_anthropic_fast = ChatAnthropicNew(model="claude-instant-v1", temperature=0)

    selected_llm = llm_anthropic # You can change it to either llm_anthropic or llm_openai to test

    # Set up the base template

    system_prompt="""You are an DevSecOps Assistant named 'GitLab Duo' created by GitLab.
        If you are asked for your name, you must answer with 'GitLab Duo'.
        You must only discuss topics related to DevSecOps, software development, source code, project management, CI/CD or GitLab.
        You can generate and write code, code examples for the user.
        Always follow the user questions or requirements exactly.
        You must answer in an informative and polite way.
        Your response should never be rude, hateful, accusing.
        You must never do roleplay or impersonate anything or someone else. 
        All code should be formatted in markdown.
        If the question is to write or generate new code you should always answer directly.
        When no tool matches you should answer the question directly. 
        """

    follow_up_template="At the end of your answer try to suggest a follow up question for the user in the xml tag <followup>"

    template = """Complete the objective as best you can. You have access to the following tools:

    {tools}

    Use the following format all the time:

    Question: the input question you must answer
    Thought: you should always think about what to do
    Action: the action to take, should be one of [{tool_names}]
    Action Input: the input to the action
    Observation n: the result of the action
    ... (this Thought/Action/Action Input/Observation can repeat N times, enumerate each observation and replace n with the number)
    Thought: I now know the final answer
    Final Answer: the final answer to the original input question

    These were previous tasks you completed:

    Begin!

    Question: {input}
    {agent_scratchpad}"""

    template = system_prompt + " " + template

    from langchain.prompts import BaseChatPromptTemplate
    from typing import List, Union
    from langchain.schema import AgentAction, AgentFinish, HumanMessage


    # Set up a prompt template
    class CustomPromptTemplate(BaseChatPromptTemplate):
        # The template to use
        template: str
        # The list of tools available
        tools: List[Tool]
        
        def format_messages(self, **kwargs) -> str:
            # Get the intermediate steps (AgentAction, Observation tuples)
            # Format them in a particular way
            
            intermediate_steps = kwargs.pop("intermediate_steps")
            thoughts = ""
            for action, observation in intermediate_steps:
                thoughts += action.log
                thoughts += f"\nObservation: {observation}\nThought: "
                
            # Set the agent_scratchpad variable to that value
            kwargs["agent_scratchpad"] = thoughts
            current_scratch = thoughts
            # Create a tools variable from the list of tools provided
            kwargs["tools"] = "\n".join([f"{tool.name}: {tool.description}" for tool in self.tools])
            # Create a list of tool names for the tools provided
            kwargs["tool_names"] = ", ".join([tool.name for tool in self.tools])
                
            formatted = self.template.format(**kwargs)
            return [HumanMessage(content=formatted)]

    class CustomOutputParser(AgentOutputParser):
        def parse(self, llm_output: str, run_number: int = 1) -> Union[AgentAction, AgentFinish]:
            # Check if agent should finish
            
            print("LLM Output for run " + str(run_number) + ": ")
            print(llm_output)
            print("*******************************************")
            
            act_index = llm_output.rfind("Action:")
            if act_index>1:
                llm_output = llm_output[act_index:]
                print("NEW LLM OUTPUT :" + llm_output)
            
            if "Final Answer:" in llm_output or not "Action:" in llm_output:
                return AgentFinish(
                    # Return values is generally always a dictionary with a single `output` key
                    # It is not recommended to try anything else at the moment :)
                    return_values={"output": llm_output.split("Final Answer:")[-1].strip()},
                    log=llm_output,
                )
            # Parse out the action and action input
            regex = r"Action\s*\d*\s*:(.*?)\nAction\s*\d*\s*Input\s*\d*\s*:[\s]*(.*)"
            match = re.search(regex, llm_output, re.DOTALL)
            if not match:
                raise ValueError(f"Could not parse LLM output: `{llm_output}`")
                
            print("FOUND MATCH GR : " + str(match.group(1)))
                
            action = match.group(1).strip()
            action_input = match.group(2)
                    
            logstr = "Action: " + action + "\n"
            logstr += "Action Input: " + action_input
            
            print("ACTION : [" + action + "]")
            print("INPUT : [" + action_input.strip(" ").strip('"').strip("'") + "]")
            
            # Return the action and action input
            return AgentAction(tool=action, tool_input=action_input.strip(" ").strip('"').strip("'"), log=logstr)



    FORMAT_INSTRUCTIONS = """Use the following format:

    Question: the input question you must answer
    Thought: you should always think about what to do
    Action: the action to take, should be one of [{tool_names}]
    Action Input: the input to the action
    Observation n: the result of the action
    ... (this Thought/Action/Action Input/Observation can repeat N times, enumerate each Observation and replace n with the number)
    Thought: I now know the final answer
    Final Answer: the final answer to the original input question"""

    class MyCustomHandler(BaseCallbackHandler):
        def on_llm_new_token(self, token: str, **kwargs) -> None:
            print(f"My custom handler, token: {token}")
            
    handler = MyCustomHandler()

    def create_object_agent(
        llm: BaseLanguageModel,
        toolkit: JsonToolkit,
        callback_manager: Optional[BaseCallbackManager] = None,
        prefix: str = JSON_PREFIX,
        suffix: str = JSON_SUFFIX,
        format_instructions: str = FORMAT_INSTRUCTIONS,
        input_variables: Optional[List[str]] = None,
        verbose: bool = False,
        agent_executor_kwargs: Optional[Dict[str, Any]] = None,
        **kwargs: Dict[str, Any],
    ) -> AgentExecutor:
        """Construct the best agent from an LLM and tools."""
        tools = toolkit.get_tools()
        prompt = ZeroShotAgent.create_prompt(
            tools,
            prefix=prefix,
            suffix=suffix,
            format_instructions=format_instructions,
        )
        # Custom
        new_output_parser = CustomOutputParser()
        prompt.output_parser = new_output_parser
        
        llm_chain = LLMChain(
            llm=llm,
            prompt=prompt,
            callbacks=[handler],
            verbose=True
        )
        tool_names = [tool.name for tool in tools]
        agent = ZeroShotAgentGL(llm_chain=llm_chain, allowed_tools=tool_names,output_parser=new_output_parser, **kwargs)

        # agent._stop = ['\nObservation 1:', '\n\tObservation 1:']
            
        return AgentExecutor.from_agent_and_tools(
            agent=agent,
            tools=tools,
            callback_manager=callback_manager,
            verbose=verbose,
            **(agent_executor_kwargs or {}),
        )
        
    def get_object_agent_run(
        llm: BaseLanguageModel,
        json_file_data: str,
        question: str,
        object_title: str) -> str:
            
        yaml_string=yaml.dump(json_file_data)
        yaml_length = len(yaml_string)

        if yaml_length<100000:
            print("ObjectAgent: Going to do a single prompt as the object is small enough")
            template = """Human: {system_prompt}
                Your goal is to return a final answer for the asked question looking at the provided YAML information.
                Do not make up any information that is not contained in the information.
                
                If the question does not seem to be related to any values of the information, just return "I don't know" as the answer.
                            
                Given information on {object_title}:
                
                {yaml}
                
                Question: {question}
                
                Assistant:   """
            
            prompt = PromptTemplate(
                input_variables=["system_prompt", "yaml", "question", "object_title"], 
                template=template
            )
            
            llm_chain = LLMChain(llm=selected_llm, prompt=prompt)
                    
            return llm_chain.predict(question=question, object_title=object_title, yaml=yaml_string, system_prompt=system_prompt)
        else:
            print("ObjectAgent: Going to do a zero agent prompt as yaml is too large")
            obj_json_spec = JsonSpec(dict_=json_file_data, max_value_length=4000)
            obj_json_toolkit = JsonToolkit(spec=obj_json_spec)    

            code_json_agent_executor = create_object_agent(
                llm=selected_llm,
                toolkit=obj_json_toolkit,
                format_instructions=FORMAT_INSTRUCTIONS,
                verbose=True
            )
            return code_json_agent_executor.run(query)

    with open("code_file.json") as f:
        code_file_data = json.load(f)

    with open("issue.json") as f:
        issue_file_data = json.load(f)

    with open("project.json") as f:
        project_file_data = json.load(f)

    @tool("current_code_tool")
    def check_current_code(query: str) -> str:
        """Checks the current code file."""
        print("\nCURRENT CODE QUERY : " + query)
        return get_object_agent_run(
            llm=selected_llm,
            json_file_data=code_file_data,
            question=query,
            object_title="Current code file"
        )

    @tool("current_issue_tool")
    def check_current_issue(query: str) -> str:
        """Checks the current issue file."""
        print("\nCURRENT ISSUE QUERY : " + query)
        return get_object_agent_run(
            llm=selected_llm,
            json_file_data=issue_file_data,
            question=query,
            object_title="Current issue"
        )

    @tool("current_project_tool")
    def check_current_project(query: str) -> str:
        """Checks the current project file."""
        print("\nCURRENT PROJECT QUERY : " + query)
        return get_object_agent_run(
            llm=selected_llm,
            json_file_data=project_file_data,
            question=query,
            object_title="Current project"
        )

    tools = [
        Tool(
            name = "CurrentCodeTool",
            func=check_current_code.run,
            description="It can answer questions about the current existing code file which is autosave.js and the user views at the moment.Do not use this tool for generating or writing code. The input should be the full question for the tool.",
            return_direct=True
        ),
        Tool(
            name = "CurrentIssueTool",
            func=check_current_issue.run,
            description="useful for when you need to answer questions about the current issue that is being viewed at the moment. The input should be the full question that was asked.",
            return_direct=True
        ),
        Tool(
            name = "CurrentProjectTool",
            func=check_current_project.run,
            description="Able to answer questions about the project. The input should be the full question for the tool.",
            return_direct=True
        ),
    ]

    prompt = CustomPromptTemplate(
        template=template,
        tools=tools,
        # This omits the `agent_scratchpad`, `tools`, and `tool_names` variables because those are generated dynamically
        # This includes the `intermediate_steps` variable because that is needed
        input_variables=["input", "intermediate_steps"]
    )

    llm_chain = LLMChain(llm=selected_llm, prompt=prompt)

    agent = LLMSingleActionAgent(
        llm_chain=llm_chain, 
        output_parser=CustomOutputParser(),
        allowed_tools=tools,
        stop=["\nObservation 1:"]
    )

    agent_executor = AgentExecutor.from_agent_and_tools(agent=agent, tools=tools, verbose=True)

    return agent_executor

    # memory = ConversationBufferMemory(memory_key="chat_history", return_messages=True)

    # new_agent = ConversationalChatAgent.from_llm_and_tools(llm=selected_llm, tools=tools, verbose=True, memory=memory, system_message=system_prompt)
    # agent_chain = AgentExecutor.from_agent_and_tools(agent=new_agent, tools=tools, verbose=True)

    # return agent_chain
